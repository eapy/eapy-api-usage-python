import requests
import os

enviroment = os.environ.get("EAPY_ENDPOINT", "http://127.0.0.1:5000")
prefix = os.environ.get("EAPY_PREFIX", "/eapy/api/2.0/")
endpoint = enviroment+prefix

payload = {
    "email":"test.api@eapy.eu",
    "password":"eapy2018"
}

login_url = endpoint + "login"

print("Executing login in "+ login_url)
print("With payload: "+ str(payload))

login_response = requests.post(login_url, json=payload)

print("Status Code: "+str(login_response.status_code))
print("Response: "+str(login_response.json()))

repository = 55
search_name = "Teste"

# TODO Change url from /data-validations to repository-check
dynamic_search_url = endpoint+"repositories/"+str(repository)+"/search/dynamic/"+search_name

payload = {
    "params": {
         "config_type": "SEARCH_OBJECT_LAMBDA"
    }
}

print("Executing dynamic in "+dynamic_search_url)
print("With payload: "+str(payload))

user = login_response.json()["response"]["user"]
auth_token = user["authentication_token"]

headers={
    "Authentication-Token": auth_token
}

response = requests.post(dynamic_search_url, json=payload, headers=headers)

print("Status Code: "+str(response.status_code))
print("Response: "+str(response.json()))
