
import requests
import os

enviroment = os.environ.get("EAPY_ENDPOINT", "http://127.0.0.1:5000")
prefix = os.environ.get("EAPY_PREFIX", "/eapy/api/2.0/")
endpoint = enviroment+prefix

payload = {
    "email":"test.api@eapy.eu",
    "password":"eapy2018"
}

login_url = endpoint + "login"

print("Executing login in "+ login_url)
print("With payload: "+ str(payload))

login_response = requests.post(login_url, json=payload)


print("Status Code: "+str(login_response.status_code))
print("Response: "+str(login_response.json()))

repository = 55
rule_name = "Teste"

expression_validation_url = endpoint+"repositories/"+str(repository)+"/business-rules/expression-validations/"+rule_name

payload = {
    "params": {
        "a": 1,
        "b": 1
    }
}

user = login_response.json()["response"]["user"]
auth_token = user["authentication_token"]

headers={
    "Authentication-Token": auth_token
}

print("Executing expression validation in "+expression_validation_url)
print("With payload: "+str(payload))

response = requests.post(expression_validation_url, json=payload, headers=headers)

print("Status Code: "+str(response.status_code))
print("Response: "+str(response.json()))
