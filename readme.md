# EAPY API DEMO with Python

The objective of this package is to create a demo using python request to access EAPY API

### Instalation

**Clone this repository**
```commandline
git clone https://gitlab.com/eapy/eapy-api-usage-python.git
```

**Note:** Git must be installed first please download and follow the instructions [here](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git).


**Create a virtual enviroment**
 ````commandline
cd eapy-api-usage-python
virtualenv .venv
source .venv/bin/active
````

**Note:** Install virtualenv and python pip before executing this command follow the instructions [here](http://timmyreilly.azurewebsites.net/python-pip-virtualenv-installation-on-windows/).
Virtual environment are recommended, however they are not mandatory to run this application. 

**Install dependencies**
```commandline
pip install -r requirements.txt
```

### Variables
The variable *EAPY_ENDPOINT* stores the api endpoint and can be changes in order to access different environments (development, quality or production).
By default the value is *"http://127.0.0.1:5000"* 

**Windows:**
```commandline
setx EAPY_ENDPOINT "http://dev.api.eapy.eu"
```

**Unix:**
````commandline
export EAPY="http://dev.api.eapy.eu"
````

### Run

**Preform Login**
```commandline
python login.py
```

**Run dynamic search**
```commandline
python run_dynamic_search.py
```

**Run expression validation**
```commandline
python run_expression_validation.py
```

**Run repository check**
```commandline
python run_repository_check.py
```
