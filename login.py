import requests
import os

enviroment = os.environ.get("EAPY_ENDPOINT", "http://127.0.0.1:5000")
prefix = os.environ.get("EAPY_PREFIX", "/eapy/api/2.0/")
endpoint = enviroment+prefix

payload = {
    "email":"test.api@eapy.eu",
    "password":"eapy2018"
}

login_url = endpoint + "login"

print("Executing login in "+ login_url)
print("With payload: "+ str(payload))

response = requests.post(login_url, json=payload)

print("Status Code: "+str(response.status_code))
print("Response: "+str(response.json()))
